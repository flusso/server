FROM node:14.4.0-alpine

MAINTAINER giovanni.puskin@gmail.com

RUN mkdir /flusso
COPY . /flusso
WORKDIR /flusso

EXPOSE 80

CMD yarn start
