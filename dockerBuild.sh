buildClient() {
  git clone git@gitlab.com:flusso/client.git --single-branch
  cd client
  yarn --production
  yarn build
}

moveClient() {
  rm -rf ../server/public
  mkdir -p ../server/public
  mv build/* ../server/public
  cd ..
  rm -rf client
}

buildBackend() {
  yarn --production
  yarn build
}

buildAndPush() {
  VERSION=$(cat package.json | sed 's/.*"version": "\(.*\)".*/\1/;t;d')
  docker build -t registry.gitlab.com/flusso/server:$VERSION .
  docker push registry.gitlab.com/flusso/server:$VERSION
}


buildClient
moveClient
buildBackend
buildAndPush