import {connectDatabase} from 'database';
import {startRestServer} from 'server';
import {loadGitConfigs} from "git";
import {initWsServer} from "websockets";

import './routes';

async function start() {
    await connectDatabase();
    await loadGitConfigs();
    await startRestServer();
    initWsServer();
}

start().then();
