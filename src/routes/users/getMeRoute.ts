import {registerRoute} from 'server';

import {getToken} from "auth/index";
import {getModel} from "database/index";

async function getMeRoute(request, response) {
    const token = request.headers?.authentication;
    const tokenFromDb = await getToken({token});

    if (!tokenFromDb) {
        throw 'Invalid token';
    }

    const user = await getModel('user').findById(tokenFromDb.entityId, {password: false});

    return response.send(user);
}

registerRoute({method: 'GET', handler: getMeRoute, url: '/me'});
