import {registerRoute} from 'server';
import {getModel} from "database";

async function updateUserRoute({params, body}, response) {
	try {
		const userId = params?.userId;

		const user = await getModel('user').update({_id: userId}, body);

		return response.send(user);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({method: 'PUT', handler: updateUserRoute, url: '/users/:userId', permissions: ['admin']});
