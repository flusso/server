import {registerRoute} from 'server'
import {getModel} from "database";

async function getUserRoute({params}, response) {
    const userId = params?.userId;

    const user = await getModel('user').findById(userId, {password: false});

    return response.send(user);
}

registerRoute({method: 'GET', handler: getUserRoute, url: '/users/:userId', permissions: ['admin']});
