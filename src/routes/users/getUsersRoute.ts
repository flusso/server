import {registerRoute} from 'server'
import {getModel} from "database";

async function getUsersRoute(_request, response) {
    const users = await getModel('user').find({}, {password: false});

    return response.send(users);
}

registerRoute({method: 'GET', handler: getUsersRoute, url: '/users', permissions: ['admin']});
