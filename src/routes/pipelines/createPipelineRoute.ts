import * as _ from 'lodash';

import {registerRoute} from 'server';
import {getModel} from "database";

import {triggerJob} from "../jobs/triggerJob";

function getFirstJobNames(flow) {
    let names: string[] = [];
    const allChildren = _.flatten(_.map(flow.jobs, 'children'));

    for (const {execution, name} of (flow.jobs || [])) {
        if (!_.includes(allChildren, name) && (!execution || execution === 'automatic')) {
            names.push(name);
        }
    }

    return names;
}

async function createPipelineRoute({params, body}, response) {
    const flowId = params?.flowId;

    if (!flowId) {
        return response.status(400).send();
    }

    const flow = await getModel('flow').findOne({_id: flowId, active: true}).lean();

    if (!flow) {
        return response.status(400).send();
    }

    const firstJobs = getFirstJobNames(flow);
    const pipeline = await getModel('pipeline').create({flowId, currentJobs: firstJobs, jobsSnapshot: flow.jobs, ...body});

    for (const job of flow.jobs) {
        delete job._id;
        triggerJob({...job, state: _.includes(firstJobs, job.name) ? 'pending' : 'created', pipelineId: pipeline._id});
    }

    return response.send(pipeline);
}

registerRoute({method: 'POST', handler: createPipelineRoute, url: '/pipelines/:flowId', permissions: ['admin']});
