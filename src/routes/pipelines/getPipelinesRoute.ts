import * as _ from 'lodash';

import {registerRoute} from 'server';
import {getModel} from "database";

async function getPipelinesRoute(_request, response) {
    const pipelines = await getModel('pipeline').find({}, {}, {sort: {lastExecution: -1}});
	let pipelinesWithFlows: any = [];

	const flowIds = _.map(pipelines, 'flowId');
	const flows = await getModel('flow').find({_id: {$in: flowIds}}, {}, {name: 1});

	for (const pipeline of pipelines) {
		pipelinesWithFlows.push({...pipeline._doc, flowName: _.get(_.find(flows, flow => flow._id.toString() === pipeline.flowId), 'name')})
	}

	return response.send(pipelinesWithFlows);
}

registerRoute({method: 'GET', handler: getPipelinesRoute, url: '/pipelines'});
