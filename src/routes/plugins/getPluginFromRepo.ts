import * as yamljs from 'yamljs';

import {getFile} from "git";
import {getModel} from "database";

type ParsedPlugin = {title: string, config: object[], envVariables: object, variables: object};
function parsePluginConfig(yml): ParsedPlugin {
    let parsed;

    try {
        parsed = yamljs.parse(yml);
    } catch (_e) {
        parsed = {};
    }
    const {title = '', config = [], envVariables = {}, variables = {}} = parsed;

    return {title, config, envVariables, variables};
}

export async function getPluginFromRepo({repoName, pluginName}): Promise<ParsedPlugin & {gitId: string, name: string}> {
    const gitQuery = repoName ? {url: repoName} : {};
    const gitConfig = await getModel('git').findOne(gitQuery);
    const {_id, url} = gitConfig;

    const pluginConfigYML = await getFile({url, branch: pluginName, fileName: 'config.yml'});
    const parsedPluginConfig = parsePluginConfig(pluginConfigYML);

    return {gitId: _id, name: pluginName, ...parsedPluginConfig};
}

