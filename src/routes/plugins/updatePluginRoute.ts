import {registerRoute} from 'server';
import {getModel} from "database";

async function updatePluginRoute({params, body}, response) {
    const pluginId = params?.pluginId;

    if (!pluginId) {
        return response.status(400).send();
    }

    await getModel('plugin').updateOne({_id: pluginId}, body);

    return response.send({});
}

registerRoute({method: 'PUT', handler: updatePluginRoute, url: '/plugins/:pluginId', permissions: ['admin']});
