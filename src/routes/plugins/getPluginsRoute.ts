import {registerRoute} from 'server';
import {getPluginNamesFromRepo} from "./getPluginNamesFromRepo";

async function getPluginsRoute(_request, response) {
	let plugins: {name: string}[] = [];

	try {
		plugins = await getPluginNamesFromRepo();
	} catch(e) {
		console.log(e)
	}

	return response.send(plugins);
}

registerRoute({method: 'GET', handler: getPluginsRoute, url: '/plugins'});
