import * as _ from 'lodash';

import {getModel} from "database";
import {getBranches} from "git";

export async function getPluginNamesFromRepo(repoName = ''): Promise<{name: string}[]> {
    let url = repoName;

    if (!url) {
        const gitModel = getModel('git');
        const gitQuery = repoName ? {url: repoName} : {};

        const gitConfig = await gitModel.findOne(gitQuery);
        url = gitConfig.url;
    }

    let branches = await getBranches(url);

    _.remove(branches, {name: 'master'})

    return branches;
}
