import {registerRoute} from 'server';
import {getModel} from "database";

async function getPluginRoute({params}, response) {
	const pluginId = params?.pluginId;

	if (!pluginId) {
		return response.status(400).send();
	}

	const plugin = await getModel('plugin').findById({_id: pluginId});

	return response.send(plugin);
}

registerRoute({method: 'GET', handler: getPluginRoute, url: '/plugins/:pluginId'});
