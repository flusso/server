import {registerRoute} from 'server';
import {getModel} from "database";

async function createPluginRoute({body}, response) {
    const plugin = await getModel('plugin').create(body);

    return response.send(plugin);
}

registerRoute({method: 'POST', handler: createPluginRoute, url: '/plugins', permissions: ['admin']});
