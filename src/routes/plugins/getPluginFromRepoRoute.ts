import {registerRoute} from 'server';
import {getPluginFromRepo} from "./getPluginFromRepo";

async function getPluginFromRepoRoute({params}, response) {
    const pluginName: string = params?.pluginName;

    if (!pluginName) {
        return response.status(400).send();
    }

    const plugin = await getPluginFromRepo({repoName: '', pluginName});

    return response.send(plugin);
}

registerRoute({method: 'GET', handler: getPluginFromRepoRoute, url: '/pluginFromRepo/:pluginName'});
