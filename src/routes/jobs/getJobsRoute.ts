import * as _ from 'lodash';

import {registerRoute} from 'server';
import {getModel} from "database";

async function getJobRoute({query}, response) {
	const pipelineId = query?.pipelineId;

	if (!pipelineId) {
		return response.status(400).send();
	}

	const jobs = await getModel('job').find({pipelineId});
	const runnerIds = _.compact(_.map(jobs, 'runnerId'));

	if (_.size(runnerIds)) {
		const runners = _.keyBy(await getModel('runner').find({_id: {$in: runnerIds}}), '_id');

		for (const jobIdx in jobs) {
			if (!jobs[jobIdx].runnerId || !runners[jobs[jobIdx].runnerId]) { continue; }

			jobs[jobIdx] = {...jobs[jobIdx]._doc, runnerName: runners[jobs[jobIdx].runnerId].name};
		}
	}

	return response.send(jobs);
}

registerRoute({method: 'GET', handler: getJobRoute, url: '/jobs'});
