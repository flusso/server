import {registerRoute} from 'server';
import {getModel} from "database";

async function getJobRoute({params}, response) {
	const jobId = params?.jobId;

	if (!jobId) {
		return response.status(400).send();
	}

	const job = await getModel('job').findById({_id: jobId});

	return response.send(job);
}

registerRoute({method: 'GET', handler: getJobRoute, url: '/jobs/:jobId'});
