import {registerRoute} from 'server';
import {getModel} from "database";

import {addRunnerAction} from "../runners/getRunnerActionsRoute";

async function cancelJobRoute({params}, response) {
    const jobId = params?.jobId;

    if (!jobId) {
        return response.status(400).send();
    }

    const {runnerId} = await getModel('job').findById(jobId);
    const {name} = await getModel('runner').findById(runnerId);

    addRunnerAction(name, {action: 'cancelJob', jobId});
    await getModel('job').update({_id: jobId}, {state: 'cancelled'});

    return response.send({});
}

registerRoute({method: 'PUT', handler: cancelJobRoute, url: '/jobs/:jobId/cancel', permissions: ['admin']});
