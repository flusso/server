import * as _ from 'lodash';

import {registerRoute} from 'server';
import {getModel} from "database/index";
import {sendWs} from "websockets/index";

async function startNextJobs(jobName, pipelineId) {
    const {currentJobs, jobsSnapshot} = await getModel('pipeline').findById(pipelineId);
    const jobs = await getModel('job').find({name: {$in: _.size(currentJobs) ? currentJobs : [jobName]}, pipelineId});

    if (!_.every(jobs, {state: 'successful'})) {
        return;
    }

    const nextJobNames = _.flatten(_.uniq(_.map(jobs, 'children')));
    const nextJobs = _.filter(jobsSnapshot, job => {
        return _.includes(nextJobNames, job.name) && (!job.execution || job.execution === 'automatic')
    });

    if (_.size(nextJobs)) {
        await getModel('job').updateMany({pipelineId, state: 'created', name: {$in: _.map(nextJobs, 'name')}}, {state: 'pending'});
    }

    return _.map(nextJobs, 'name');
}

async function finishJobRoute({params, body}, response) {
    const jobId = params?.jobId;

    if (!jobId) {
        return response.status(400).send();
    }

    const jobModel = getModel('job');
    const pipelineModel = getModel('pipeline');

    const {exitCode = 0} = body;
    const jobState = exitCode === 0 ? 'successful' : 'failed';
    await jobModel.update({_id: jobId}, {state: jobState, finished: new Date().toISOString()});

    let nextJobs;

    const {name, pipelineId} = await jobModel.findById(jobId);

    if (exitCode === 0) {
        nextJobs = await startNextJobs(name, pipelineId);
    }
    const ongoingJobsInPipeline = await jobModel.find({state: {$in: ['created', 'pending', 'running']}, pipelineId});

    let updateObject: {state: string, currentJobs?: string[]} = {
        state: _.some(ongoingJobsInPipeline, job => _.includes(['running', 'pending'], job.state)) ? 'paused' : 'finished',
    };

    if (nextJobs) {
        updateObject.currentJobs = nextJobs;
    }

    await pipelineModel.update({_id: pipelineId}, updateObject);

    if (updateObject.state === 'finished') {
        sendWs(pipelineId, {action: 'pipelineFinished'});
    }

    return response.send({});
}

registerRoute({method: 'PUT', handler: finishJobRoute, url: '/finishJob/:jobId', permissions: ['admin', 'runner']});
