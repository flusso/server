import {registerRoute} from 'server';
import {getModel} from "database";
import {sendWs} from "websockets/index";

async function updateJobRoute({params, body}, response) {
    const jobId = params?.jobId;

    if (!jobId) {
        return response.status(400).send();
    }

    await getModel('job').update({_id: jobId}, body);

    if (body?.$push?.logs) {
        const {pipelineId} = await getModel('job').findById(jobId);

        sendWs(pipelineId, {action: 'logsReceived', jobId, logs: [body?.$push?.logs]});
    }

    return response.send({});
}

registerRoute({method: 'PUT', handler: updateJobRoute, url: '/jobs/:jobId', permissions: ['admin', 'runner']});
