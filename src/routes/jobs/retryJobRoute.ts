import {registerRoute} from 'server';
import {getModel} from "database";
import {sendWs} from "websockets/index";

async function updateJobRoute({params}, response) {
    const jobId = params?.jobId;

    if (!jobId) {
        return response.status(400).send();
    }

    const jobModel = getModel('job');
    const {tags, pluginIds, children, shell, execution, name, image, scripts, pipelineId, executionParentId} = await jobModel.findById(jobId);

    const newJob = await jobModel.create({
		state: 'pending',
		tags,
		pluginIds,
		children,
		shell,
		execution,
		name,
		image,
		scripts,
		pipelineId,
		executionParentId: executionParentId || jobId
    });

    sendWs(pipelineId, {action: 'jobRetried', jobId: newJob._id});

    return response.send(newJob);
}

registerRoute({method: 'PUT', handler: updateJobRoute, url: '/jobs/:jobId/retry', permissions: ['admin']});
