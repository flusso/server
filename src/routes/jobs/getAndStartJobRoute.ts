import * as _ from 'lodash';

import {registerRoute} from 'server';
import {getModel} from "database";

import {getRunner} from "../runners/getRunner";
import {sendWs} from "websockets";

async function getAndStartJobRoute({params}, response) {
	const runnerName = params?.runnerName;

	if (!runnerName) {
		return response.status(400).send();
	}

	const runner = await getRunner({name: runnerName});

	if (!runner) {
		return response.status(400).send();
	}

	await getModel('runner').update({_id: runner._id}, {lastContact: new Date()});

	const jobModel = getModel('job');
	const pipelineModel = getModel('pipeline');
	const pluginModel = getModel('plugin');

	const {tags = []} = runner;

	const job = await jobModel.findOneAndUpdate({state: 'pending', $or: [{tags: {$size: 0}}, {tags: {$in: tags}}]}, {state: 'running', started: new Date().toISOString(), runnerId: runner._id}, {sort: {created: -1}});
	let plugins: {gitId?: string, _doc?: object, git?: object}[] = [];
	let returnObj = {};

	if (job) {
		pipelineModel.update({_id: job.pipelineId, state: 'running'});

		if (job.pluginIds[0]) {
			plugins = await pluginModel.find({_id: {$in: job.pluginIds}});
			const gitIds = _.compact(_.map(plugins, 'gitId'));
			const gitConfigs = await getModel('git').find({_id: {$in: gitIds}});

			for (const plugin in plugins) {
				if (!plugins[plugin].gitId) { continue; }
				const gitConfig = _.find(gitConfigs, config => config._id.toString() === plugins[plugin].gitId);

				plugins[plugin] = {...plugins[plugin]._doc, git: gitConfig._doc};
			}
		}

		returnObj = {...job._doc, plugins};
		sendWs(job.pipelineId, {action: 'jobStarted', name: job.name});
	}

	return response.send(returnObj);
}

registerRoute({method: 'GET', handler: getAndStartJobRoute, url: '/getAndStartJob/:runnerName', permissions: ['runner']});
