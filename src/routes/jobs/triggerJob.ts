import {getModel} from "database";

export function triggerJob(job) {
    return getModel('job').create(job);
}
