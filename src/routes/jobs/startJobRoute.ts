import {registerRoute} from 'server';
import {getModel} from "database";

async function startJobRoute({params}, response) {
    const jobId = params?.jobId;

    if (!jobId) {
        return response.status(400).send();
    }

    await getModel('job').update({_id: jobId}, {state: 'pending'});

    return response.send({});
}

registerRoute({method: 'PUT', handler: startJobRoute, url: '/jobs/:jobId/start', permissions: ['admin']});
