import {registerRoute} from 'server';
import {getModel} from "database";

async function updateVariablesRoute({params, body}, response) {
	try {
		const variableId = params?.variableId;

		const variable = await getModel('variable').update({_id: variableId}, body);

		return response.send(variable);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({method: 'PUT', handler: updateVariablesRoute, url: '/variables/:variableId', permissions: ['admin']});
