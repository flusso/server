import {registerRoute} from 'server';
import {getModel} from "database";

async function deleteVariableRoute({params}, response) {
    const variableId = params?.variableId;

    if (!variableId) {
        return response.status(400).send();
    }

    await getModel('variable').remove({_id: variableId});

    return response.send({});
}

registerRoute({method: 'DELETE', handler: deleteVariableRoute, url: '/variables/:variableId', permissions: ['admin']});
