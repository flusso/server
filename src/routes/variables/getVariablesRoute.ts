import {registerRoute} from 'server'
import {getModel} from "database";

async function getVariablesRoute(_request, response) {
    const variables = await getModel('variable').find({});

    return response.send(variables);
}

registerRoute({method: 'GET', handler: getVariablesRoute, url: '/variables', permissions: ['admin']});
