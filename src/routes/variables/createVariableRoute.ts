import {registerRoute} from 'server';
import {getModel} from "database";

async function createVariableRoute({body}, response) {
    const variable = await getModel('variable').create(body);

    return response.send(variable);
}

registerRoute({method: 'POST', handler: createVariableRoute, url: '/variables'});
