import * as bcrypt from 'bcrypt';

import {registerRoute} from 'server';
import {getModel} from "database";
import {tokenModel} from "auth/tokenModel";
import {createToken} from "auth/createToken";

async function loginRoute({body}, response) {
    const {account, password} = body;

    if (!account || !password) {
        return response.status(400).send();
    }

    const user = await getModel('user').findOne({active: true, $or: [{email: account}, {username: account}]});

    if (!user) {
        return response.status(401).send();
    }

    const samePassword = await bcrypt.compare(password, user.password);

    if (!samePassword) {
        return response.status(401).send();
    }

    await tokenModel.findOneAndUpdate({entityId: user._id, active: true}, {active: false});
    const token = await createToken({entityId: user._id, type: 'user'});

    return response.send({token: token.token});
}

registerRoute({method: 'POST', handler: loginRoute, url: '/login', auth: false});
