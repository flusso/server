import * as _ from 'lodash';

import {registerRoute} from 'server';
import {getModel} from "database";

async function getFlowsRoute(_request, response) {
    let flows = await getModel('flow').find({}, {}, {sort: {created: -1}});
    let flowsWithPipelines: any = [];
    const flowIds = _.map(flows, '_id');
    const pipelines = _.groupBy(await getModel('pipeline').find({state: {$in: ['running', 'pending']}, flowId: {$in: flowIds}}), 'flowId');

    for (const flow of flows) {
        flowsWithPipelines.push({
			...flow._doc,
			runnningPipelines: _.size(_.filter(pipelines[flow._id], {state: 'running'})),
			pendingPipelines: _.size(_.filter(pipelines[flow._id], {state: 'pending'})),
        });
    }

    return response.send(flowsWithPipelines);
}

registerRoute({method: 'GET', handler: getFlowsRoute, url: '/flows'});
