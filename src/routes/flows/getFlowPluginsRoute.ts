import * as _ from 'lodash';

import {registerRoute} from "server";
import {getModel} from "database";

async function getFlowPluginsRoute({params}, response) {
    const flowId = params?.flowId;

    const flow = await getModel('flow').findById(flowId) || {};
    const pluginIds = _.flatten(_.map(flow.jobs, 'pluginIds'));
    const plugins = await getModel('plugin').find({_id: {$in: pluginIds}});

    return response.send(plugins);
}

registerRoute({method: 'GET', handler: getFlowPluginsRoute, url: '/flows/:flowId/plugins'});
