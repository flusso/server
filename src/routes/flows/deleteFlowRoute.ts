import {registerRoute} from 'server';
import {getModel} from "database";

async function deleteFlowRoute({params}, response) {
    const flowId = params?.flowId;

    if (!flowId) {
        return response.status(400).send();
    }

    await getModel('flow').remove({_id: flowId});
    await getModel('pipeline').remove({flowId: flowId});

    return response.send({});
}

registerRoute({method: 'DELETE', handler: deleteFlowRoute, url: '/flows/:flowId', permissions: ['admin']});
