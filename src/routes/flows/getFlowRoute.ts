import {registerRoute} from 'server';
import {getModel} from "database";

async function getFlowRoute({params}, response) {
    const flowId = params?.flowId;

    const flow = await getModel('flow').findById(flowId);

    return response.send(flow);
}

registerRoute({method: 'GET', handler: getFlowRoute, url: '/flows/:flowId'});
