import {registerRoute} from 'server';
import {getModel} from "database";

async function updateFlowRoute({params, body}, response) {
    const flowId = params?.flowId;

    if (!flowId) {
        return response.status(400).send();
    }

    await getModel('flow').updateOne({_id: flowId}, body);

    return response.send({});
}

registerRoute({method: 'PUT', handler: updateFlowRoute, url: '/flows/:flowId', permissions: ['admin']});
