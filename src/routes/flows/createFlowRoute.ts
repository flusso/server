import {registerRoute} from 'server';
import {getModel} from "database";

async function createFlowRoute({body}, response) {
    const flow = await getModel('flow').create(body);

    return response.send(flow);
}

registerRoute({method: 'POST', handler: createFlowRoute, url: '/flows'});
