import * as mongoose from 'mongoose';

import {registerRoute} from 'server';
import {getModel} from "database";

async function getRunnerRoute({params}, response) {
	try {
		const runnerIdOrName = params?.runnerIdOrName;
		const query = mongoose.Types.ObjectId.isValid(runnerIdOrName) ? {_id: runnerIdOrName} : {name: runnerIdOrName};

		const runner = await getModel('runner').findOne(query);

		return response.send(runner);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({method: 'GET', handler: getRunnerRoute, url: '/runners/:runnerIdOrName', permissions: ['admin', 'runner']});
