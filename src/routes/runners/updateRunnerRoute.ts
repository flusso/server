import {registerRoute} from 'server';
import {getModel} from "database";

async function updateRunnerRoute({params, body}, response) {
	try {
		const runnerId = params?.runnerId;

		const runner = await getModel('runner').update({_id: runnerId}, body);

		return response.send(runner);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({method: 'PUT', handler: updateRunnerRoute, url: '/runners/:runnerId', permissions: ['admin', 'runner']});
