import {registerRoute} from 'server';
import {getToken} from 'auth';

import {getRunner} from './getRunner';
import {getModel} from "database/index";

async function registerRunnerRoute({body, ip}, response) {
	try {
		const {runner = {}, runnerRegistrationToken} = body;

		if (!runnerRegistrationToken) {
			return response.status(400).send({message: 'missing runnerRegistrationToken'});
		}

		const token = await getToken({type: 'runnerRegistrationToken'});

		if (!token) {
			return response.status(500).send({message: 'the server is not configured yet!'});
		}

		if (token.token !== runnerRegistrationToken) {
			return response.status(401).send({message: 'invalid runnerRegistrationToken'});
		}

		if (runner.name && await getRunner({name: runner.name})) {
			return response.status(400).send({message: 'a runner with this name already exists!'});
		}

		const newRunner = await getModel('runner').create({...runner, ip});

		return response.send(newRunner);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({method: 'POST', handler: registerRunnerRoute, url: '/runners', auth: false});
