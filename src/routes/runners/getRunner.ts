import {getModel} from "database";

export function getRunner(query:object = {}) {
	return getModel('runner').findOne({active: true, ...query});
}
