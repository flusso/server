import * as _ from 'lodash';

import {registerRoute} from 'server';

import {getRunner} from "./getRunner";

let runnerActions: {runnerName: string; action: object}[] = [];

export function addRunnerAction(runnerName, action) {
	runnerActions.push({runnerName, action});
}

async function getRunnerActionsRoute({params}, response) {
	try {
		const runnerName = params?.runnerName;
		const runner = await getRunner({name: runnerName});

		if (!runner) {
			return response.status(400).send();
		}

		const actions = _.remove(runnerActions, {runnerName});

		return response.send(actions);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({
	method: 'GET',
	handler: getRunnerActionsRoute,
	url: '/getRunnerActionsRoute/:runnerName',
	permissions: ['runner']
});
