import {registerRoute} from 'server';
import {getModel} from "database";

async function getRunnersRoute(request, response) {
	try {
		const runners = await getModel('runner').find(request.params);

		return response.send(runners);
	} catch ({message}) {
		return response.status(400).send({message});
	}
}

registerRoute({method: 'GET', handler: getRunnersRoute, url: '/runners', permissions: ['admin', 'runner']});
