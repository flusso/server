import {registerRoute} from "server/index";
import {getToken} from "auth/index";

async function getRunnerRegistrationTokenRoute(_request, response) {
    const {token} = await getToken({type: 'runnerRegistrationToken'});

    return response.send({token});
}

registerRoute({method: 'GET', handler: getRunnerRegistrationTokenRoute, url: '/runnerRegistrationToken', permissions: ['admin']});