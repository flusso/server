import * as bcrypt from 'bcrypt';

import {registerRoute} from 'server';
import {getModel} from "database";

function isUsernameInvalid(username) {
	const isValid = username.length >= 3;

	if (!isValid) {
		return 'At least 3 characters please!';
	}

	return false;
}

function isEmailInvalid(email) {
	const isValid = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email);

	if (!isValid) {
		return 'Invalid email format!';
	}

	return false;
}

function arePasswordInvalid(password, repeatPassword) {
	const areMatching = password && repeatPassword && password === repeatPassword;
	const longEnough = password.length >= 8;

	if (!areMatching) {
		return 'The passwords are not matching!';
	}

	if (!longEnough) {
		return 'At least 8 characters please!';
	}


	return false;
}

async function singupRoute({body}, response) {
	try {
		const {email, username, password, repeatPassword} = body;

		let errorMessage;

		errorMessage = isEmailInvalid(email);
		errorMessage = errorMessage || isUsernameInvalid(username);
		errorMessage = errorMessage || arePasswordInvalid(password, repeatPassword);

		if (errorMessage) {
			return response.status(400).send({message: errorMessage});
		}

		const model = getModel('user');

		const user = await model.findOne({active: true, $or: [{email}, {username}]});

		if (user) {
			return response.status(400).send({message: 'user already exists'});
		}

		const encryptedPassword = await bcrypt.hash(password, 10);

		await model.create({email, username, password: encryptedPassword});

		return response.send({});
	} catch (e) {
		return response.status(400).send({message: e});
	}
}

registerRoute({method: 'POST', handler: singupRoute, url: '/signup', auth: false});
